import { Component } from '@angular/core';
import { MovieService } from './movies/movie.service';

@Component({
  selector: 'app-root',
  template: `
  <div>
  <h1> Komponenta </h1>
  <movies></movies>
  </div>`,
  styleUrls: ['./app.component.css'],
  providers: [MovieService]
})
export class AppComponent {
  title = 'app';
}
