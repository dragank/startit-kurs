
import { Movie } from "./movie.model";
import { Component, OnInit } from "@angular/core";
import { MovieService } from "./movie.service";
import { error } from "util";



@Component({
    selector: 'movies',
    templateUrl: './movie-list.component.html'
})
export class MovieListComponent implements OnInit{
    private naslov: string = 'Movie Lista';
    prikaziLink: boolean = true;
    dinamickiTekst: string = 'inicijalni';
    errorMessage: string;
    // movies: any[] = [
     movies: Movie[] = [];
    getNaslov(): string {
        return this.naslov;
    }

    prikaziLinkNaTabeli() {
        this.prikaziLink = !this.prikaziLink;
    }

    constructor(private _movieService: MovieService) {
        this._movieService = _movieService;
    }

    ngOnInit(): void {
        this._movieService.getMovies()
        .subscribe(movies => this.movies = movies,
                    error => this.errorMessage = <any> error);
                    
    }

}