import { Movie } from "./movie.model";
import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";


@Injectable()
export class MovieService {
    private _movieUrl = 'https://my-json-server.typicode.com/dkrstevski/demo/movies';
    constructor(private _http: HttpClient) {
    }

    getMovies(): Observable<Movie[]> {
        return this._http.get<Movie[]>(this._movieUrl);
    }
}