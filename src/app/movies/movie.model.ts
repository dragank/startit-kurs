
export interface Movie {
    movieId: number;
    movieName: string;
    genre: string;
    duration: number;
    releaseDate: Date;
    rating: number;
    linkUrl: string;
}